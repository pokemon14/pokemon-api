package com.pokemonapi.controllers;

import com.pokemonapi.entities.Pokemon;
import com.pokemonapi.entities.PokemonDetails;
import com.pokemonapi.services.PokemonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/api/pokemons")
public class PokemonController {

    @Autowired
    private PokemonService pokemonService;

    @GetMapping
    public List<Pokemon> allPokemons(@RequestParam(value = "offset", defaultValue = "0", required = false) int offset,
                                     @RequestParam(value = "limit", defaultValue = "20", required = false) int limit) {
        return pokemonService.getAllPokemons(offset, limit);
    }

    @GetMapping("/{name}")
    public PokemonDetails pokemon(@PathVariable(name = "name") String name) {
        return pokemonService.getPokemon(name);
    }
}
