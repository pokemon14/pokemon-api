package com.pokemonapi.entities;

public class Pokemon {

    private int id;
    private String name;
    private String url;

    private boolean published = true;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public Pokemon(String name, String url, boolean published) {
        super();
        this.name = name;
        this.url = url;
        this.published = true;
    }

    public Pokemon() {
        super();
    }
}
