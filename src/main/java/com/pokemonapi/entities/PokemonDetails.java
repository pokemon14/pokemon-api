package com.pokemonapi.entities;

public class PokemonDetails {

    private int id;
    private int base_experience;
    private int height;
    private int weight;
    private int order;
    private String name;
    private String location_area_encounters;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBase_experience() {
        return base_experience;
    }

    public void setBase_experience(int base_experience) {
        this.base_experience = base_experience;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLocation_area_encounters() {
        return location_area_encounters;
    }

    public void setLocation_area_encounters(String location_area_encounters) {
        this.location_area_encounters = location_area_encounters;
    }

    public PokemonDetails(int id, int base_experience, int height, int weight, int order, String name, String location_area_encounters) {
        super();
        this.id = id;
        this.base_experience = base_experience;
        this.height = height;
        this.weight = weight;
        this.order = order;
        this.name = name;
        this.location_area_encounters = location_area_encounters;
    }

    public PokemonDetails() {
        super();
    }
}
