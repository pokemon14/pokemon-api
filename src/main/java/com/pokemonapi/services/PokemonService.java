package com.pokemonapi.services;

import com.pokemonapi.entities.Pokemon;
import com.pokemonapi.entities.PokemonDetails;
import com.pokemonapi.entities.PokemonResponse;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class PokemonService {

    public List<Pokemon> getAllPokemons(int offset, int limit) {
        String url = "https://pokeapi.co/api/v2/pokemon-species" + "?limit=" + limit + "&offset=" + offset;
        PokemonResponse pokemonResponse = consulExtAll(url);
        return pokemonResponse.getResults();
    }

    private PokemonResponse consulExtAll(String url) {
        RestTemplate restTemplate = new RestTemplate();
        PokemonResponse pokemonResponse = restTemplate.getForObject(url, PokemonResponse.class);
        return pokemonResponse;
    }

    public PokemonDetails getPokemon(String name) {
        String url = "https://pokeapi.co/api/v2/pokemon/" + name;
//        PokemonResponse pokemonResponse = consulExtSpecified(url);
        return consulExtSpecified(url);
    }

    private PokemonDetails consulExtSpecified(String url) {
        RestTemplate restTemplate = new RestTemplate();
        PokemonDetails pokemonDetails = restTemplate.getForObject(url, PokemonDetails.class);
        return pokemonDetails;
    }
}
